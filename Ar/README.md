# extend the system

you will use nfold command. check Ar-nfold.control

for 2 2 2 that will generate a system 8 times bigger, two times in each direction.

DLPOLY.Z -c Ar-nfold.control -o nfold.log

you will get two new files

Ar.config_2_2_2
Ar.field_2_2_2

these files you will use in Ar.control to run a "benchmarking calculation for new system"

look for

io_file_config Ar.config_2_2_2
io_file_field Ar.field_2_2_2

to run

DLPOLY.Z -c Ar.control -o Ar.log

